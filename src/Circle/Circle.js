import React, {Component} from 'react';
import {
    View
    , Text
    , StyleSheet
    , Dimensions
} from 'react-native';


class Circle extends Component {

    constructor(props) {
        super(props);
        this.state = this.getDimensions()
    }

    getDimensions(){
        let {height, width} = Dimensions.get('window')
        return {
            height
            , width
        }
    }

    componentWillMount() {
        Dimensions.addEventListener('change', ()=> {
            this.setState(this.getDimensions())
        })
    }

    render() {
        return (
            <View style={[styles.container, this.state]}>
                <View style={styles.innerContainer}>
                    <Text style={styles.text}>{this.props.text}</Text>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center'
        , justifyContent: 'center'
    },
    innerContainer: {
        alignItems: 'center'
        , justifyContent: 'center'
        , height: 150
        , width: 150
        , borderRadius: 150
        , backgroundColor: '#2e4e5b'
    },
    text: {
        fontSize: 50
        , color: '#d74f41'
    }
})


export default Circle;