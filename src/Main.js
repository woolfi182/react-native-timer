import React, {Component} from 'react';
import {
    View
    , Text
    , Modal
    , StyleSheet
    , TextInput
    , Vibration
    , Alert
} from 'react-native';
import Sound from 'react-native-sound';
import SplashScreen from 'react-native-splash-screen'

import Circle from './Circle/Circle'
import Button from './Button/Button'
import ModalInput from './ModalInput/ModalInput'

class Main extends Component {
    constructor(props) {
        super(props);
        this.state = {
            interval: null
            , time: 10
            , currentTime: 0
            , visible: false
        };
    }

    startTimer(time) {
        const interval = setInterval(() => {
            if (this.state.currentTime !== 0 && !((this.state.currentTime+1) % time)) {
                Vibration.vibrate();
                this.playSound()
            }
            this.setState({
                currentTime: this.state.currentTime + 1
            })
        }, 1000);

        this.setState({
            interval: interval
            , time
            , visible: false
        })
    }

    stopTimer(changeState) {
        clearInterval(this.state.interval);
        if (changeState) this.setState({interval: null, currentTime: 0});
    }

    playSound() {
        const sound = new Sound('advertising.wav', Sound.MAIN_BUNDLE, error => {
            if (error) {
                Alert.alert('error', error.message);
                return;
            }
            sound.play(() => {
                sound.release();
            });
        })
    }

    render() {
        let buttonText = this.state.interval ? 'Stop' : 'Input the time';
        let buttonFunction = () => {
            this.state.interval ? this.stopTimer(true) : this.setState({visible: true});
        };
        return (
            <View style={styles.container}>
                <Circle
                    text={String(this.state.currentTime)}
                />
                <Button
                    text={buttonText}
                    click={()=>buttonFunction()}
                />
                <ModalInput
                    visible={this.state.visible}
                    setInputData={time => this.startTimer(time)}
                />
            </View>
        )
    }

    componentWillUnmount() {
        this.stopTimer(false)
    }

    componentDidMount() {
        SplashScreen.hide();
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
        , flexDirection: 'column'
        , justifyContent: 'flex-end'
        , backgroundColor: '#1c2f36'
    },
})

export default Main;