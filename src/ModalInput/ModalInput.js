import React, {Component, PropTypes} from 'react';
import {
    Modal
    , View
    , StyleSheet
    , Text
    , TextInput
    , TouchableOpacity
} from 'react-native';


class Circle extends Component {

    constructor(props) {
        super(props);
        this.state = {
            value: null
        }
    }

    closeModal() {
        let value = this.state.value ? this.state.value : this.props.placeholder;
        this.setState({
            value: null
        });
        this.props.setInputData(value);
    }

    onChangeInput(value) {
        if (/\d/.test(value[value.length - 1]) || !value) {
            this.setState({
                value: value
            })
        }
    }

    render() {
        return (
            <Modal
                animationType="fade"
                transparent={true}
                visible={this.props.visible}
                onRequestClose={() => this.closeModal()}
            >
                <View style={styles.modalInputContainer}>
                    <Text style={styles.inputTopText}>Input the value of time:</Text>
                    <View style={styles.modalInput}>
                        <TextInput
                            style={styles.input}
                            timeValue={true}
                            keyboardType="numeric"
                            maxLength={2}
                            caretHidden={true}
                            autoFocus={true}
                            placeholder={this.props.placeholder.toString()}
                            value={this.state.value}
                            underlineColorAndroid={'transparent'}
                            onChangeText={text => this.onChangeInput(text)}
                        />
                    </View>
                    <TouchableOpacity
                        onPress={()=>this.closeModal()}
                        style={styles.button}
                    >
                        <Text style={styles.text}>Start Timer</Text>
                    </TouchableOpacity>
                </View>
            </Modal>
        )
    }
}
Circle.propTypes = {
    setInputData: PropTypes.func.isRequired
    , visible: PropTypes.bool.isRequired
    , placeholder: PropTypes.number
}

Circle.defaultProps = {
    placeholder: 10
}

const styles = StyleSheet.create({

    modalInputContainer: {
        flex: 1
        , flexDirection: 'column'
        , justifyContent: 'center'
        , backgroundColor: 'rgba(52, 75, 88, 0.7)'
        , alignItems: 'center'
    },
    modalInput: {
        backgroundColor: '#7eacc4'
        , borderStyle: 'solid'
        , borderWidth: 2
        , borderColor: '#5b8da6'
        , borderRadius: 20
        , width: 100
        , alignItems: 'center'
    },
    input: {
        width: 43
        , textAlign: 'center'
        , color: '#d84c47'
        , fontSize: 30
    },
    inputTopText: {
        fontSize: 20
        , marginBottom: 20
        , textAlign: 'center'
        , color: '#7eacc4'
    },
    button: {
        width: 170
        , height: 45
        , marginTop: 30
        , backgroundColor: '#dc4c46'
        , borderColor: '#a03025'
        , borderRadius: 15
        , borderWidth: 1
    },
    text: {
        fontSize: 30
        , textAlign: 'center'
        , color: '#1c2f36'
    }
})


export default Circle;