import React, {Component} from 'react';
import {
    View
    , Text
    , StyleSheet
    , TouchableOpacity
} from 'react-native';


class Button extends Component {
    render() {
        return (
            <TouchableOpacity
                style={styles.container}
                onPress={()=>this.props.click()}
            >
                <Text style={styles.text}>{this.props.text}</Text>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#253b46'
        , alignItems: 'center'
        , justifyContent: 'center'
        , height: 50
    },
    text: {
        color: '#90959d'
        , fontSize: 20
    }
})


export default Button;